package TeaMaking;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TeaMake {

    public static void main(String[] args) throws InterruptedException {
        Tea tea = new Tea();

        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(new BoilOff(tea));
        service.execute(new BoilOn(tea));

        TimeUnit.SECONDS.sleep(5);
        service.shutdownNow();
    }


}
