package TeaMaking;

import java.util.concurrent.TimeUnit;

public class BoilOff implements Runnable {

    private Tea tea;

    public BoilOff(Tea tea) {
        this.tea = tea;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                tea.waitForBoiling();
                System.out.println("Boil off!");
                TimeUnit.MILLISECONDS.sleep(500);
                tea.filledCup();
            } catch (InterruptedException e) { }
        }

        System.out.println("Ended " + Thread.currentThread().getName());
    }



}
