package TeaMaking;

import java.util.concurrent.TimeUnit;

public class BoilOn implements Runnable {

    private Tea tea;

    public BoilOn(Tea tea) {
        this.tea = tea;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                System.out.println("Boil On!");
                TimeUnit.MILLISECONDS.sleep(500);
                tea.boiled();
                tea.filling();
            } catch (InterruptedException e) { }
        }

        System.out.println("Ended " + Thread.currentThread().getName());
    }
}
