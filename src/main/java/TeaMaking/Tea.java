package TeaMaking;

public class Tea {

    private boolean isBoiled = false;

    public synchronized void boiled() {
        isBoiled = true;
        notifyAll();
    }

    public synchronized void filledCup() {
        isBoiled = false;
        notifyAll();
    }

    public synchronized void waitForBoiling() throws InterruptedException {
        while (!isBoiled) {
            wait();
        }
    }

    public synchronized void filling() throws InterruptedException {
        while (isBoiled) {
            wait();
        }
    }

}
