package Fibonacci;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Fibonacci implements Runnable {

    private int number;
    private ArrayList<Integer> list;

    public Fibonacci(int number) {
        this.number = number;
        list = new ArrayList<>();
    }

    private synchronized void solving() {
        int t1 = 0;
        int t2 = 1;

        for(int i = 1; i <= number; i++) {
            list.add(t1);

            int temp = t1 + t2;
            t1 = t2;
            t2 = temp;

            if(t1 > number) {
                return;
            }
        }
    }

    @Override
    public void run() {
        solving();
        synchronized (this) {
            for(int i: list) {
                System.out.print(i + " ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Fibonacci fib = new Fibonacci(23);
        Fibonacci fib1 = new Fibonacci(22);
        Fibonacci fib2 = new Fibonacci(21);

        ExecutorService service = Executors.newSingleThreadExecutor();
        service.execute(fib);
        service.execute(fib1);
        service.execute(fib2);

        TimeUnit.SECONDS.sleep(2);
        service.shutdownNow();
    }
}
