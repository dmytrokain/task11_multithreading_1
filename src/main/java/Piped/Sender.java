package Piped;

import java.io.PipedWriter;
import java.util.concurrent.TimeUnit;

public class Sender implements Runnable {

    private PipedWriter out = new PipedWriter();
    public PipedWriter getPipedWriter() { return out; }

    @Override
    public void run() {
        try {
            while (true) {
                for(char c = 'a'; c <= 'z'; c++) {
                    out.write(c);
                    TimeUnit.MILLISECONDS.sleep(500);
                }
            }
        } catch (Exception e) { }
   }
}
