package PingPong;

public class Player {

    private boolean ball = false;

    public synchronized void repeledBall() {
        ball = true;
        notifyAll();
    }

    public synchronized void noRepealBall() {
        ball = false;
        notifyAll();
    }

    public synchronized void waitingForBall() throws InterruptedException {
        while (!ball) {
            wait();
        }
    }

    public synchronized void repealing() throws InterruptedException {
        while (ball) {
            wait();
        }
    }
}
