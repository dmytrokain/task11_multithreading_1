package PingPong;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game {

    public static void main(String[] args) {

        Player player = new Player();

        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(new Ping(player));
        service.execute(new Pong(player));

    }


}
