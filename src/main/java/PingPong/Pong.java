package PingPong;

import java.util.concurrent.TimeUnit;

public class Pong implements Runnable{

    Player player;

    public Pong(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                player.waitingForBall();
                System.out.println("Pong");
                TimeUnit.SECONDS.sleep(2);
                player.noRepealBall();

            }
        } catch (InterruptedException e) {}
    }
}
