package PingPong;

import java.util.concurrent.TimeUnit;

public class Ping implements Runnable {

    Player player;

    public Ping(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                player.repealing();
                System.out.println("Ping");
                TimeUnit.SECONDS.sleep(2);
                player.repeledBall();
            } catch (InterruptedException e) { }
        }
    }
}
